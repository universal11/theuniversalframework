<?php
define("FRAMEWORK_DIR", dirname(__FILE__) . "/");
include(FRAMEWORK_DIR . "core/DbConnection.php");
include(FRAMEWORK_DIR . "core/UniversalModel.php");
include(FRAMEWORK_DIR . "core/UniversalController.php");
include(FRAMEWORK_DIR . "core/UniversalApiController.php");



$dbConnection = new DbConnection("localhost", "root", "root");

$request = (object)$_REQUEST;
$debugMode = ( (empty($request->debug)) ? false : $request->debug );
if($debugMode){
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}




//
// Route Handler
//

if(!empty($request->controller)){
	$controller = $request->controller;
	$boot = APP_DIR . "controllers/{$controller}.php";
	//echo "Booting: $boot<br>\r\n";
	if(file_exists($boot)){
		//echo "Including: $boot<br>\r\n";
		include($boot);
		if(class_exists($controller)){
			//echo "Initializing controller...<br>\r\n";
			$controller = new $controller($dbConnection);
			if(!empty($request->action)){
				$action = $request->action;
				if(method_exists($controller, $action)){
					//echo "Calling action...<br>\r\n";
					$controller->{$action}($request);
				}
				
			}
			else{
				$controller->main($request);
			}
		}
	}
	
}


?>