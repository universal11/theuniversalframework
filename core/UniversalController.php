<?php

class UniversalController extends UniversalModel{
	public $dbConnection;
	public function __construct($dbConnection = false){
		if($dbConnection){
			$this->dbConnection = $dbConnection;
		}
	}

	public function main($request){
		print_r($request);
		echo "<br><br>Built by the UniversalFramework.<br>\r\n";
	}

	public function import($package, $libraryDirectory = false){
		if(!$libraryDirectory){ $libraryDirectory = APP_DIR; }
		$package = str_replace(".", "/", $package);
		$package = "{$libraryDirectory}{$package}.php";
		include($package);
	}

	public function loadClient($data){
		$controller = $this->getParameter($data->request->controller);
		$action = $this->getParameter($data->request->action);

		$this->loadClientView($controller, $action);
		$this->loadClientController($data);
	}

	public function loadClientController($data){
		$controller = $this->getParameter($data->request->controller);
		$action = $this->getParameter($data->request->action);
		if(!$action){ $action = "main"; }
		$requestToJson = json_encode($data);
		
		$this->loadClientScript(".theuniversalframework.core.scripts.UniversalModel", true);
		$this->loadClientScript(".theuniversalframework.core.scripts.UniversalController", true);

		echo "<script type=\"text/javascript\" language=\"javascript\" src=\"controllers/{$controller}.js\"></script>\r\n"; 
		
		echo "<script type=\"text/javascript\" language=\"javascript\">\r\n";
		echo "var controller = new {$controller}();\r\n";
		$this->import("interface.{$controller}.{$action}.script");
		echo "\r\n";
		echo "controller.{$action}({$requestToJson});\r\n";
		echo "</script>\r\n";
	}

	public function loadClientScript($package){
		$package = str_replace(".", "/", $package);
		$package = "{$package}.js";
		echo "<script type=\"text/javascript\" language=\"javascript\" src=\"{$package}\"></script>\r\n";
	}

	public function loadClientView($controller, $action){
		$package = "interface.{$controller}.{$action}.styles";
		$this->import("core.app", FRAMEWORK_DIR);
		$this->loadClientStyles($package, false);
		
	}

	public function loadClientStyles($package){
		$package = str_replace(".", "/", $package);
		$package = "{$package}.php";
		echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"{$package}\"/>\r\n";
	}


	public function getParameter($parameter = false){
		return ( (!$parameter) ? false : $parameter);
	}

}

?>