function UniversalController(){
	UniversalModel.call(this);
	this.data = new Object();
	console.log("Universal Controller Activated!!!");
}

UniversalController.prototype = Object.create(UniversalModel.prototype);

UniversalController.prototype.construct = UniversalController;

UniversalController.prototype.main = function(){

}

UniversalController.prototype.createEvent = function(eventName){
	var eventObject = new CustomEvent(eventName);
	document.dispatchEvent(eventObject);
}

UniversalController.prototype.loadData = function(data){
	this.data = data;
	this.createEvent("update");
}

UniversalController.prototype.addEventListener = function(eventName, eventHandler){
	document.addEventListener(eventName, eventHandler, false);
}

UniversalController.prototype.setData = function(data){
	this.data = data;
}

