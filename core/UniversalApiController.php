<?php

class UniversalApiController extends UniversalController{
	public function __construct(){
	}

	public function response($data){
		echo json_encode($data);
	}

	public function getParameter($parameter = false){
		return ( (!$parameter) ? false : $parameter);
	}
}

?>