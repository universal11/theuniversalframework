<?php

class DbConnection{
	private $host;
	private $user;
	private $password;
	private $connection;

	public function __construct($host, $user, $password){
		//echo "DB connection spawned!";
		$this->host = $host;
		$this->user = $user;
		$this->password = $password;
		//echo "Opening DB connection...";
		$this->open();
		//echo "db connection opened...";
	}

	public function open(){
		$this->connection = mysql_connect($this->host, $this->user, $this->password) or die(mysql_error());
	}

	public function close(){
		mysql_close($this->connection);
	}

	public function query($sql){
		//echo "Performing query...";
		$result = mysql_query($sql, $this->connection) or die(mysql_error());
		//echo "query ran...";
		$data = false;
		//echo "Retrieving results...";
		while($row = mysql_fetch_object($result)){
			$data[] = $row;
		}
		//echo "query complete...";
		return $data;
	}

	public function queryUniqueResult($sql){
		$result = mysql_query($sql, $this->connection) or die(mysql_error());
		$data = null;
		while($row = mysql_fetch_object($result)){
			$data = $row;
		}
		return $data;
	}

	public function execute($sql){
		$result = mysql_query($sql, $this->connection) or die(mysql_error());
		return $result;
	}

}


?>