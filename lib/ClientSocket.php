<?php

class ClientSocket{
	private $id;
	private $message;
	private $host;
	private $port;
	private $socket;
	private $outbound;
	private $inbound;
	private $errorNumber;
	private $errorMessage;


	public function __construct($host, $port){
		//eventually want to add unique id for each socket
		print_r($host);
		$this->host = $host;
		$this->port = $port;
	}

	public function getErrorNumber(){
		return $this->errorNumber;
	}

	public function connect(){
		$this->socket = stream_socket_client("tcp://{$this->host}:{$this->port}", $this->errorNumber, $this->errorMessage);
		if ($this->socket === false) {
		    throw new UnexpectedValueException("Failed to connect: {$this->errorMessage}");
		}
	}

	public function close(){
		fclose($this->socket);
	}

	public function transmit($data){
		$this->outbound = fwrite($this->socket, $data);
	}

	public function receive(){
		$this->inbound = stream_get_contents($this->socket);
		return $this->inbound;
	}

	public function getId(){
		return $this->id;
	}

	public function setMessage($message){
		$this->message = $message;
	}

	public function getMessage(){
		return $this->message;
	}
}

?>