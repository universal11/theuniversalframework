<?php

class TcpProtocol{
	private $protocolType;
	private $data;
	public function __construct($protocolType, $data){
		$this->protocolType = $protocolType;
		$this->data = $data;
	}

	public function getProtocolType(){
		return $this->protocolType;
	}

	public function getData(){
		return $this->data;
	}
}

?>