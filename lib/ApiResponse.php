<?php

class ApiResponse{
	public $success;
	public $data;
	public $message;
	public $code;

	public function __construct(){
		$this->success = false;
		$this->message = null;
		$this->data = null;
		$this->code = 500;
	}

	public function init($success, $data, $message, $code){
		$this->success = $success;
		$this->data = $data;
		$this->message = $message;
		$this->code = $code;
	}

	public function setSuccess($success){
		$this->success = $success;
	}

	public function setMessage($message){
		$this->message = $message;
	}

	public function setData($data){
		$this->data = $data;
	}

	public function setCode($code){
		$this->code = $code;
	}

	public function isSuccess(){
		return $this->success;
	}

	public function getMessage(){
		return $this->message;
	}

	public function getData(){
		return $this->data;
	}

	public function getCode(){
		return $this->code;
	}
}

?>